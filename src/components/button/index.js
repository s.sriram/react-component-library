import React from "react";
import PropTypes from "prop-types";

const Button = ({ disabled, backgroundColor, color, children, ...rest }) => {
  return (
    <button
      disabled={disabled}
      {...rest}
      style={{ color: color, backgroundColor: backgroundColor }}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  disabled: PropTypes.bool,
};

export default Button;
