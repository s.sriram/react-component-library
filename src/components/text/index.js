import React from "react";
import PropTypes from "prop-types";

const Text = ({ content }) => {
  return <span style={{ fontWeight: 500 }}>{content}</span>;
};

Text.propTypes = {};

export default Text;
